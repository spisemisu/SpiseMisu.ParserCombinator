/* Version from Robert (rycee): */
{ pkgs ? import <nixpkgs> {
  overlays = [(
    self: super: {
      dotnet-sdk-specific = super.dotnet-sdk.overrideAttrs (
        old: rec {
          version = "8.0.100";
          src = super.fetchurl {
            url = "https://dotnetcli.azureedge.net/dotnet/Sdk/${version}/dotnet-sdk-${version}-linux-x64.tar.gz";
            sha256 = "550692ae3233e69692822b8be31bb01c5a2b60c3cfa59e9bfce8278b99f36211";
          };
        }
      );
    }
  )];
}}:

with pkgs;

mkShell {
  buildInputs = [
    ncurses # for `clear`
    emacs
    openssl
    zlib
    dotnet-sdk-specific
  ];
  shellHook = ''
    # Add alias for the F# Compiler
    alias fsc="dotnet ${dotnet-sdk-specific.out}/sdk/${dotnet-sdk-specific.version}/FSharp/fsc.dll"
    # ~/.dotnet/tools/csharp-ls
    export PATH="$PATH:/home/johndoe/.dotnet/tools"
    export CLI_DEBUG=1
    export DOTNET_CLI_TELEMETRY_OPTOUT="1"
    export DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=1
    export LD_LIBRARY_PATH=${openssl.out}/lib/:${zlib.out}/lib/
    # export LD_DEBUG="all"
    #
    # Issue with: FAKE + FSAC and dotnet 8.0.x
    # - https://github.com/fsprojects/fantomas/issues/412#issuecomment-798774514
    #
    #```sh
    # echo "# Get repo and place in fsac"
    # git clone \
    #     --depth 1 \
    #     --branch v0.71.0 \
    #     https://github.com/fsharp/FsAutoComplete.git fsac
    # 
    # echo "# Ensure folder exists in .emacs and clear it"
    # mkdir -p   ~/.emacs.d/FsAutoComplete/
    # rm    -frv ~/.emacs.d/FsAutoComplete/
    # 
    # echo "# Build .NET core binaries and place in emacs folder"
    # cd fsac/src/FsAutoComplete
    # dotnet tool restore
    # export BuildNet8=true && dotnet build --configuration Release
    # 
    # echo "# Place binaries in emacs folder"
    # cd bin/Release/net8.0/
    # mkdir -p ~/.emacs.d/FsAutoComplete/netcore/
    # cp -Rv * ~/.emacs.d/FsAutoComplete/netcore/
    # 
    # echo "# Done"
    # ```
    #
    # ```text
    # "Ensure that `defun eglot-fsharp--maybe-install` looks like:"
    # "(defun eglot-fsharp--maybe-install ()"
    # "  ())"
    # "in `~/.emacs.d/elpa/eglot-fsharp-…/eglot-fsharp.el` and then:"
    # "M-x byte-recompile-directory"
    # "Byte recompile directory: ~/.emacs.d/elpa/eglot-fsharp-…/"
    #
    # (defun eglot-fsharp--maybe-install (&optional version)
    # "  ())"
    #    
    # So for script now `eglot` has to be `M-x`
    #```
    #
    #```
    # ~/.emacs.d/FsAutoComplete/netcore/./fsautocomplete
    #```
    # If it fails, search for `-dotnet-sdk-8.0.100` in `/nix/store` like:
    #
    #```
    # find /nix/store -name "*-dotnet-sdk-8.0.100"
    #```
    #
    export DOTNET_ROOT="${dotnet-sdk-specific.out}"
  '';  
}

# References:
#
# Download .NET (specific):
#
# - https://dotnet.microsoft.com/download/dotnet
#
# - https://dotnet.microsoft.com/download/dotnet/8.0
#
# Invariant mode (Environment variable):
#
# - https://learn.microsoft.com/en-us/dotnet/core/runtime-config/globalization#invariant-mode
