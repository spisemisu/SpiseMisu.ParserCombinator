#!/bin/sh

# Clean screen
clear

# Build in Release mode
env DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=0 \
    dotnet build --configuration Release

# References
# ==========
# 
# - `dotnet build`
# https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-build
#
# - Runtime configuration options for globalization
# https://learn.microsoft.com/en-us/dotnet/core/runtime-config/globalization#invariant-mode
