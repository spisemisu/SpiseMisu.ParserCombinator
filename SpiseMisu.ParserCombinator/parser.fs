namespace SpiseMisu

module Parser =
  
  open System
  
  (* Types *)
  [<RequireQualifiedAccess>]
  module Source =
    
    type t = private | Source of string
    
    let fromString x = Source x
    
    let len (Source x) = String.length x
    
    let get i j (Source x) =
      let n = String.length x - 1
      let l = 0 <= i && 0 <= j
      let u = i <= n && j <= n
      if l && u then
        Some x.[i .. j]
      else
        None
  
    let idx off (pattern : string) (Source x) =
      match x.IndexOf (value = pattern, startIndex = off) with
        | -1 -> None
        | nr -> Some nr
  
  [<RequireQualifiedAccess>]
  module State =
    
    type t =
      { off : int
      ; src : Source.t
      }
  
  [<RequireQualifiedAccess>]
  module Step =
    
    type 'a t =
      | Okay of
          value : 'a *
          state : State.t
      | Fail of
          error : string
  
  type 'a parser =
    | Parser of (State.t -> 'a Step.t)
  
  [<RequireQualifiedAccess>]
  module Trampoline =
    
    type ('s, 'a) thunk =
      | Done of 'a
      | Jump of 's
    and ('s, 'a) t = 's -> thunk<'s, 'a> parser
    
    let loop (ts : 's) (tp : t<'s, 'a>) : 'a parser =
      let rec aux ts0 s0 =
        let (Parser p) = tp ts0
        match p s0 with
          | Step.Okay (step, s1) ->
            match step with
              | Jump ts1 ->
                aux ts1 s1
              | Done a   ->
                Step.Okay (a, s1)
          | Step.Fail e          ->
            Step.Fail e
      fun s -> aux ts s
      |> Parser

  (* Functor *)
  let fmapP (f:'a -> 'b) (Parser p:'a parser) : 'b parser =
    ( fun s0 ->
        match p s0 with
          | Step.Okay (a, s1) -> Step.Okay (f a, s1)
          | Step.Fail  e      -> Step.Fail    e
    )
    |> Parser
  
  let (<!>) : ('a -> 'b) -> 'a parser -> 'b parser = fmapP

  (* Applicative *)
  let pureP (a : 'a) : 'a parser =
    fun s -> Step.Okay (a, s)
    |> Parser
  
  let lift2AP
    (f : 'a -> 'b -> 'c)
    (Parser pA:'a parser)
    (Parser pB:'b parser) : 'c parser =
    ( fun s0 ->
        match pA s0 with
          | Step.Okay (a, s1) ->
            match pB s1 with
              | Step.Okay (b, s2) ->
                Step.Okay (f a b, s2)
              | Step.Fail e       ->
                Step.Fail e
          | Step.Fail e       ->
            Step.Fail e
    )
    |> Parser
  
  let (<*>) (pF:('a -> 'b) parser) (pA:'a parser) : 'b parser =
    lift2AP (<|) pF pA
  
  let ( *>) (pA:'a parser) (pB:'b parser) : 'b parser =
    lift2AP (fun _ x -> x) pA pB
  
  let (<* ) (pA:'a parser) (pB:'b parser) : 'a parser =
    lift2AP (fun x _ -> x) pA pB

  (* Monad *)
  let bindP (Parser pA: 'a parser) (f: 'a -> 'b parser) : 'b parser =
    ( fun s0 ->
        match pA s0 with
          | Step.Okay (a, s1) ->
            let (Parser pB) = f a
            pB s1
          | Step.Fail e       ->
            Step.Fail e
    )
    |> Parser
  
  let joinP (Parser pp:'a parser parser) : 'a parser =
    ( fun s0 ->
        match pp s0 with
          | Step.Okay ((Parser p), s1) ->
            p s1
          | Step.Fail e                ->
            Step.Fail e
    )
    |> Parser
  
  let (>>=) : 'a parser -> ('a -> 'b parser) -> 'b parser = bindP

  (* Alternate *)
  let (<|>) (Parser p1:'a parser) (Parser p2:'a parser) : 'a parser =
    ( fun s ->
        match p1 s with
          | Step.Okay _ as step1 ->
            step1
          | Step.Fail _          ->
              match p2 s with
                | Step.Okay _ as step2 ->
                  step2
                | Step.Fail _ as step2 ->
                  step2
    )
    |> Parser

  (* Error *)
  let error (s : State.t) msg : string =
    let n = Source.len s.src - 1
    let i = s.off
    let r =
      Source.get i n s.src
      |> Option.defaultValue String.Empty
    sprintf
      "Parser error\n\
       * Message...........: %s\n\
       * Offset............: %i\n\
       * Unparsed string...: %A\n"
      msg i r

  (* Parsers *)
  let getP : string parser =
    ( fun (s : State.t) ->
        match Source.get s.off s.off s.src with
          | Some c ->
            Step.Okay (c, { s with off = s.off + 1 })
          | None   ->
            error s "getP"
            |> Step.Fail
    )
    |> Parser
  
  let lookP n : string parser =
    ( fun (s : State.t) ->
        let i = s.off
        let j = i + n - 1
        match Source.get i j s.src with
          | Some c ->
            Step.Okay (c, s)
          | None   ->
            error s "lookP"
            |> Step.Fail
    )
    |> Parser
  
  let failP () : 'a parser =
    ( fun s ->
        error s "failP"
        |> Step.Fail
    )
    |> Parser
  
  let eosP : unit parser =
    ( fun (s : State.t) ->
        let n = Source.len s.src
        let i = s.off
        if i = n then
          Step.Okay ((), s)
        else
          error s "eosP"
          |> Step.Fail
    )
    |> Parser
  
  let satisfyP f : string parser =
    ( fun (s : State.t) ->
        match Source.get s.off s.off s.src with
          | Some cs ->
            if f cs.[0] then
              Step.Okay (cs, { s with off = s.off + 1 })
            else
              "satisfyP > different char"
              |> error s
              |> Step.Fail
          | None    ->
            "satisfyP > not enough chars"
            |> error s
            |> Step.Fail
    )
    |> Parser
  
  let untilP (pattern : string) : string parser =
    ( fun (s : State.t) ->
        match Source.idx s.off pattern s.src with
          | Some n ->
            let j = n - 1
            match Source.get s.off j s.src with
              | Some cs ->
                Step.Okay (cs, { s with off = n })
              | None    -> 
                error s "untilP"
                |> Step.Fail
          | None ->
            error s "untilP"
            |> Step.Fail
    )
    |> Parser
  
  let charP (c:char) : string parser =
    ( fun (s : State.t) ->
        let i = s.off
        let j = s.off
        match Source.get i j s.src with
          | Some cs ->
            if cs = string c then
              Step.Okay (cs, { s with off = s.off + 1 })
            else
              "charP > different char"
              |> error s
              |> Step.Fail
          | None   ->
            "charP > no char"
            |> error s
            |> Step.Fail
    )
    |> Parser
  
  let stringP (x:string) : string parser =
    ( fun (s : State.t) ->
        let n = String.length x
        let i = s.off
        let j = i + n - 1
        match Source.get i j s.src with
          | Some cs ->
            if cs = x then
              Step.Okay (cs, { s with off = s.off + n })
            else
              "stringP > different string"
              |> error s
              |> Step.Fail
          | None   ->
            "stringP > not enough chars"
            |> error s
            |> Step.Fail
    )
    |> Parser
  
  let munch1P f : string parser =
    let rec aux (s : State.t) =
      match Source.get s.off s.off s.src with
        | Some cs ->
          if f cs.[0] then
            aux { s with off = s.off + 1 }
          else
            s
        | None ->
          s
    ( fun s ->
        let e = aux s
        let i = s.off
        let j = e.off - 1 (* Last char failed, so skip it *)
        match Source.get i j s.src with
          | Some cs when cs <> "" ->
            Step.Okay (cs, e)
          | _____________________ ->
            error s "munch1P"
            |> Step.Fail
    )
    |> Parser
    
  let munchP f : string parser =
    munch1P f <|> pureP ""
  
  let skipSpacesP : unit parser =
    ( fun _ -> ()
    )
    <!> munchP Char.IsWhiteSpace
    
  let choiceP (ps : 'a parser list) : 'a parser =
    let rec aux s ps' =
      match ps' with
        | [              ] ->
          error s "choiceP"
          |> Step.Fail
        | (Parser p) :: rs ->
          match p s with
            | Step.Okay _  as step ->
              step
            | Step.Fail _          ->
              aux s rs
    ( fun s ->
        aux s ps
    )
    |> Parser
  
  let seqP p : ('a seq parser) =
    let rec aux acc =
      choiceP
        [ ( fun x ->
              x :: acc
              |> Trampoline.Jump
          )
          <!> p
        ; ( fun _ ->
              acc
              |> List.rev
              |> List.toSeq
              |> Trampoline.Done
          )
          <!> pureP ()
        ]
    Trampoline.loop [] aux
  
  let many1P (p : 'a parser) : 'a seq parser =
    ( fun x xs ->
        ( Seq.singleton x
        , xs
        ) ||> Seq.append
    )
    <!> p
    <*> seqP p
  
  let manyP (p : 'a parser) : 'a seq parser =
    choiceP 
      [ many1P p
      ; pureP Seq.empty
      ]
  
  let sepBy1P p sep : ('a seq parser) =
    ( fun x xs ->
        ( Seq.singleton x
        , xs
        ) ||> Seq.append
    )
    <!> p
    <*> (seqP (sep *> p))
  
  let sepByP p sep : ('a seq parser) =
    choiceP
      [ sepBy1P p sep
      ; pureP Seq.empty
      ]
      
  let spaceP : string parser =
    satisfyP Char.IsWhiteSpace
  
  let spacesP : string parser =
    munch1P Char.IsWhiteSpace
  
  let digitP : string parser =
    satisfyP Char.IsDigit
  
  let digitsP : string parser =
    munch1P Char.IsDigit
  
  let hexDigitP : string parser =
    ( fun c ->
        ('0' <= c && c <= '9') ||
        ('a' <= c && c <= 'f') ||
        ('A' <= c && c <= 'F')
    )
    |> satisfyP
  
  let hexDigitsP : string parser =
    ( fun c ->
        ('0' <= c && c <= '9') ||
        ('a' <= c && c <= 'f') ||
        ('A' <= c && c <= 'F')
    )
    |> munch1P

  (* Run *)
  let deferP (thunk : unit -> 'a parser) : 'a parser =
    ( fun s ->
        let (Parser p) = thunk ()
        p s
    )
    |> Parser
  
  let runP (Parser p : 'a parser) (str : string) : Result<'a, string> =
    match p { off = 0; src = Source.fromString str } with
      | Step.Okay (a, s) when Source.len s.src = s.off ->
        Result.Ok a
      | Step.Okay (a, s)                               ->
        error s "runP"
        |> Result.Error
      | Step.Fail  e                                   ->
        Result.Error e
