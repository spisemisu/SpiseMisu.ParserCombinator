#!/bin/sh

clear

ver="$1"

dotnet add dummy.fsproj \
       package SpiseMisu.ParserCombinator \
       --version ${ver}
