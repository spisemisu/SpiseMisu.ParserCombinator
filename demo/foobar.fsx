#!/usr/bin/env -S dotnet fsi --langversion:8.0 --optimize --warnaserror+:25,26

//#I @"../SpiseMisu.ParserCombinator/bin/Release/net8.0/"
//#r @"SpiseMisu.ParserCombinator.dll"

#r "nuget: SpiseMisu.ParserCombinator, 00.11.15"

#time "on"

open System

open SpiseMisu.Parser

module rec FooBar =
  
  type t =
    | Foo of t seq
    | Bar of int
  
  let foobarP () =
        barP
    <|> fooP
  
  let barP =
    ( int >> Bar
    )
    <!> digitsP
  
  let foobarsP =
    sepByP
      ( deferP foobarP )
      ( skipSpacesP *> charP ',' <* skipSpacesP )
  
  let fooP =
    ( Foo
    )
    <!> charP '[' *> skipSpacesP *> foobarsP <* skipSpacesP <* charP ']'

open FooBar

let _ =
  
  "[\t00, [ \n1337,2     , 42]     ]"
  
  (* Should be parsed as:
     > Foo (seq [Bar 0; Foo (seq [Bar 1337; Bar 2; Bar 42])])
  *)
  
  |> runP (deferP foobarP)
  |> function
    | Ok    a -> printfn "# Parse:\n%A" a
    | Error e -> printfn "# Error:\n%s" e
  
  00
